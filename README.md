#TP #3 CAR : Akka
Par Alexis BEAUJET et Joël Ilunga Katumba

Avant toute chose, il arrive que des messages se perdent et arrivent en DeadLetter lorsqu'on lance le programme pour la première fois. Lors du second lancement et pour les suivants, l'erreur ne se produit plus.
Je n'ai pas trouvé de documentation à ce sujet. (il y en a en Scala, mais apparemment pas applicables à la version Java...) 

Q1. Tests présents dans le projet.

Q2. On commence par construire un arbre d'acteurs Akka. Pour cela on crée les acteurs, puis on définit les relations de parenté dans l'arbre sous la forme d'une liste de AkkaRelationship.
Lorsqu'on lance la construction de l'arbre, des messages sont envoyés à chacun des noeuds, spécifiant quel objet adopter comme père ou enfants.

Q3. Lorsqu'on injecte un message dans l'arbre via un noeud, le message est transmis au parent du noeud, mais aussi aux enfants. Ensuite, chaque noed rép-te l'opération en prenant soin de ne pas renvoyer le message à son expéditeur.

Q4. Ajout de la méthode addForeignRelationship pour joindre deux noeuds de deux systèmes différents.
Je ne suis pas parvenu à obtenir un déploiement correct avec le fichier de configuration et les adresses Akka :( Les deux systèmes communiquent mais sur la même machine.

Q5. Ajout de AkkaMessage.initiator pour ne pas boucler dans le graph. On vérifie à chaque relayage qu'on n'envoie pas le message à l'initateur, ainsi on casse les éventuelles boucles.
