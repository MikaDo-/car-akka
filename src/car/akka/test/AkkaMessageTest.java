package car.akka.test;

import junit.framework.TestCase;
import ms2.car.akka.AkkaMessage;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;

/**
 * Created by joel on 3/30/16.
 */
public class AkkaMessageTest extends TestCase{

    @Test
    public void testExistAkkaMessage(){
        AkkaMessage akkaMessage = new AkkaMessage();
        assert(akkaMessage.getClass().getName().contains("AkkaMessage"));
    }

    @Test
    public void testNotNullAkkaMessage(){
        AkkaMessage akkaMessage = new AkkaMessage();
        assertNotNull(akkaMessage);
    }
}