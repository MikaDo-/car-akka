package car.akka.test;

import akka.actor.ActorRef;
import akka.actor.ActorRefScope;
import akka.actor.ActorSystem;
import akka.actor.Props;
import ms2.car.akka.AkkaNode;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by joel on 3/30/16.
 */
public class AkkaNodeTest {

    @Test
    public void testGetName() throws Exception {
        ActorSystem actorSystem = ActorSystem.create("mySystemServer");
        ActorRef actorRef = actorSystem.actorOf(Props.create(AkkaNode.class), "myActor");

        assertNotNull(actorRef);
    }

    @Test
    public void testOnReceive() throws Exception {

    }
}