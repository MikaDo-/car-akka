package car.akka.test;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import ms2.car.akka.AkkaNode;
import ms2.car.akka.AkkaRelationship;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by joel on 3/30/16.
 */
public class AkkaRelationshipTest {

    @Test
    public void testGetFather() throws Exception {
        ActorSystem mySystem = ActorSystem.create("mysystemtest");
        ActorRef father = mySystem.actorOf(Props.create(AkkaNode.class), "father");
        ActorRef son = mySystem.actorOf(Props.create(AkkaNode.class), "son");

        AkkaRelationship relationship = new AkkaRelationship(father, son);

        assertNotNull(relationship);
        assert(relationship.getFather().getClass().equals(father.getClass()));
        assert(relationship.getFather() != relationship.getSon());

    }

    @Test
    public void testGetSon() throws Exception {
        ActorSystem mySystem = ActorSystem.create("mysystemtest");
        ActorRef father = mySystem.actorOf(Props.create(AkkaNode.class), "father");
        ActorRef son = mySystem.actorOf(Props.create(AkkaNode.class), "son");

        AkkaRelationship relationship = new AkkaRelationship(father, son);

        assertNotNull(relationship);
        assert(relationship.getFather().getClass().equals(father.getClass()));
        assert(relationship.getFather() != relationship.getSon());
    }
}