package car.akka.test;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import ms2.car.akka.AkkaTree;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by joel on 3/30/16.
 */
public class AkkaTreeTest {

    @Test
    public void testCreateNode() throws Exception {
        ActorSystem actorSystem = ActorSystem.create("mySystemServer");
        ActorRef ref = new AkkaTree(actorSystem).createNode("mytestAkkatree");

        assertNotNull(ref);
    }

    @Test
    public void testAddNode() throws Exception {
        ActorSystem actorSystem = ActorSystem.create("mySystemServer");
        AkkaTree myTree = new AkkaTree(actorSystem);
        ActorRef ref = myTree.createNode("mytestnode");

        myTree.addNode("father", ref);

        assert(myTree.getActors() != null);
        assert(myTree.getActors().size() != 0);
    }

    @Test
    public void testBuildTree() throws Exception {

    }

    @Test
    public void testAddRelationship() throws Exception {

    }
}