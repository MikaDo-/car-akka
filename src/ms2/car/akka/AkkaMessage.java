package ms2.car.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import java.io.Serializable;

/**
 * Created by alexis on 29/03/16.
 */
public class AkkaMessage implements Serializable, Cloneable{
    public enum AkkaMessageDirection{
        AKKA_MESSAGE_DIRECTION_UP, AKKA_MESSAGE_DIRECTION_DOWN, AKKA_MESSAGE_DIRECTION_DIRECT
    }
    private ActorRef from;
    private ActorRef to;
    private ActorRef initiator; // pour ne pas boucler dans un graph
    private String message;
    private AkkaMessageDirection direction;

    public AkkaMessage() {
        this.from = null;
        this.to = null;
        this.message = "<unset>";
        this.direction = AkkaMessageDirection.AKKA_MESSAGE_DIRECTION_DIRECT;
    }

    public AkkaMessage(ActorRef from, ActorRef to, String message, AkkaMessageDirection direction) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.direction = direction;
        this.initiator = null;
    }

    // constructeur par copie
    public AkkaMessage(AkkaMessage mess){
        this.from = mess.from;
        this.to = mess.to;
        this.message = mess.message;
        this.direction = mess.direction;
        this.initiator = mess.initiator;
    }

    public AkkaMessageDirection getDirection() {
        return direction;
    }

    public ActorRef getInitiator() {
        return initiator;
    }

    public void setInitiator(ActorRef initiator) {
        this.initiator = initiator;
    }

    public void setDirection(AkkaMessageDirection direction) {
        this.direction = direction;
    }

    public String getMessage() {
        return message;
    }

    public AkkaMessage clone(){
        return new AkkaMessage(this);
    }
}
