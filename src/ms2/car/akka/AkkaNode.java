package ms2.car.akka;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;

import java.util.ArrayList;

/**
 * Created by alexis on 29/03/16.
 */
public class AkkaNode extends UntypedActor {

    protected String name;
    protected ArrayList<ActorRef> children;
    protected ActorRef parent;

    public AkkaNode() {
        this.children = new ArrayList<>();
    }

    public AkkaNode(ActorRef parent, String name) {
        this.children = new ArrayList<>();
        this.parent = parent;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void onReceive(Object o) throws Exception {
        // s'il s'agit d'une définition de filiation :
        if(o instanceof AkkaRelationship){
            AkkaRelationship rel = (AkkaRelationship) o;
            if(rel.getSon() == getSelf())
                this.parent = rel.getFather();
            if(rel.getFather() == getSelf())
                this.children.add(rel.getSon());
        }
        // si c'est un simple message :
        else if(o instanceof AkkaMessage) {
            AkkaMessage m = (AkkaMessage)o;

            // si le message vient d'être injecté dans l'arbre, on devient l'initiateur de la diffusion aux autres.
            if(m.getDirection() == AkkaMessage.AkkaMessageDirection.AKKA_MESSAGE_DIRECTION_DIRECT)
                m.setInitiator(getSelf());

            System.out.println(getSelf() + " says : " + m.getMessage());

            if((m.getDirection() == AkkaMessage.AkkaMessageDirection.AKKA_MESSAGE_DIRECTION_UP
                    || m.getDirection() == AkkaMessage.AkkaMessageDirection.AKKA_MESSAGE_DIRECTION_DIRECT) && this.parent != null) {
                if(this.parent != m.getInitiator()) {
                    AkkaMessage m2 = m.clone();
                    m2.setDirection(AkkaMessage.AkkaMessageDirection.AKKA_MESSAGE_DIRECTION_UP);
                    this.parent.tell(m2, getSelf());
                }
            }
            for (ActorRef child : this.children) {
                if (getSender() != child && child != m.getInitiator()) { // on ne veut pas boucler indéfiniment lors de la propagation de messages dans l'arbre
                    AkkaMessage m2 = m.clone();
                    m2.setDirection(AkkaMessage.AkkaMessageDirection.AKKA_MESSAGE_DIRECTION_DOWN);
                    child.tell(m2, getSelf());
                }
            }
        }
    }
}
