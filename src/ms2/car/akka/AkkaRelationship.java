package ms2.car.akka;

import akka.actor.ActorRef;

/**
 * Created by alexis on 30/03/16.
 */

public class AkkaRelationship {
    protected ActorRef father;
    protected ActorRef son;

    public AkkaRelationship(ActorRef father, ActorRef son) {
        this.father = father;
        this.son = son;
    }

    public ActorRef getFather() {
        return father;
    }
    public ActorRef getSon() {
        return son;
    }
}
