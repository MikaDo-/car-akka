package ms2.car.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by alexis on 30/03/16.
 */
public class AkkaTree {

    protected ActorSystem system;
    protected HashMap<String, ActorRef> actors;
    protected ArrayList<AkkaRelationship> relationships;

    public AkkaTree(ActorSystem system) {
        this.system = system;
        this.actors = new HashMap<>();
        this.relationships = new ArrayList<>();
    }

    public ActorRef createNode(String nodeName){
        ActorRef ref = this.system.actorOf(Props.create(AkkaNode.class), nodeName);
        actors.put(nodeName, ref);
        return ref;
    }

    public void addNode(String nodeName, ActorRef node){
        actors.put(nodeName, node);
    }

    // Les acteurs sont créés. Maintenant, il faut leur dire comment s'agencer en arbre. Pour cela, il faut envoyer
    // un message à chaque noeud père avec la liste des enfants qu'il doit prendre dans le système.
    public void buildTree() {
        for (AkkaRelationship rel : this.relationships) {
            rel.getFather().tell(rel, rel.getSon());
            rel.getSon().tell(rel, rel.getFather());
        }
    }

    public void addRelationship(String father, String son){
        this.relationships.add(new AkkaRelationship(this.system.actorFor("user/"+father), this.system.actorFor("user/"+son)));
    }

    public void addForeignRelationship(String father, String son, ActorSystem fatherSystem, ActorSystem sonSystem){
        this.relationships.add(new AkkaRelationship(fatherSystem.actorFor("user/"+father), sonSystem.actorFor("user/"+son)));
    }

    public HashMap<String, ActorRef> getActors(){
        return this.actors;
    }

    public ArrayList<AkkaRelationship> getRelationships(){
        return this.relationships;
    }
}
