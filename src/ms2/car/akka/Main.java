package ms2.car.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import java.util.HashMap;

public class Main {

    static HashMap<String, ActorRef> nodes;
    static ActorSystem system;
    static AkkaTree tree;
    static ActorSystem system2;
    static AkkaTree tree2;

    public static void main(String[] args) {
        // Le but ici est de construire le système d'acteurs Akka, puis un arbre basé sur ce système.
        Main.system = ActorSystem.create("system1");
        Main.tree = new AkkaTree(Main.system);
        Main.system2 = ActorSystem.create("system2");
        Main.tree2 = new AkkaTree(Main.system2);
        Main.nodes = new HashMap<>();

        // Ajout des acteurs dans l'arbre (ce dernier crée l'acteur, et l'ajoute donc dans le système).
        for (int i = 1;i<=4;i++) {
            String nodeName = "node"+i;
            Main.nodes.put(nodeName, tree.createNode(nodeName));
        }
        Main.nodes.put("node5", tree2.createNode("node5"));
        Main.nodes.put("node6", tree2.createNode("node6"));

        // On définie les relations de parenté
        tree.addRelationship("node1", "node2");
        tree.addForeignRelationship("node1", "node5", system, system2);

        tree.addRelationship("node2", "node3");
        tree.addRelationship("node2", "node4");

        tree2.addRelationship("node5", "node6");

        // Q5 . on passe en graph
        tree.addForeignRelationship("node4", "node6", system, system2);

        // On met à jour la topologie.
        tree.buildTree();

        // envoie de messages depuis la racine, vers les enfants.
        ActorRef from = Main.nodes.get("node1");
        AkkaMessage mess = new AkkaMessage(ActorRef.noSender(), from, "message", AkkaMessage.AkkaMessageDirection.AKKA_MESSAGE_DIRECTION_DIRECT );
        from.tell(mess, from);

        // on tue les systèmes
        Main.system.shutdown();
        Main.system2.shutdown();
    }

}
